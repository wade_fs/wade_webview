package com.wade.webview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by wade on 2018/1/26.
 */

public class HtmlEditActivity extends AppCompatActivity {
    private HtmlEditActivity context;
    private String url = "";
    LineNumberEditText lineNumberEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_html_edit);
        context = this;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent rIntent = getIntent();
        Uri uri = rIntent.getData();
        if (uri == null) {
            url = rIntent.getStringExtra("url");
            if (url == null) url = "";
        } else {
            url = uri.getPath();
        }
        lineNumberEditText = (LineNumberEditText)findViewById(R.id.lineNumberEditText);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (url.length() == 0) {
            lineNumberEditText.setText("這是測試內容\n您不應該看到此內容\n");
        } else {
            lineNumberEditText.setText(readText(url));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_source, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.save_file:
                try {
                    File save = new File(url);
                    save.createNewFile();
                    FileOutputStream out = new FileOutputStream(save);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);
                    outputStreamWriter.append(lineNumberEditText.getText().toString());
                    outputStreamWriter.close();
                    out.close();
                } catch (FileNotFoundException e) {
                    Log.d("MyLog", "Cannot save to file "+url+": "+e.getMessage());
                } catch (Exception e) {
                    Log.d("MyLog", "Cannot save to file "+url+": "+e.getMessage());
                }
                return true;
        }
        return false;
    }

    private String readText(String filename) {
        File f = new File(filename);
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
            return byteArrayOutputStream.toString();
        } catch (IOException e) {
            Log.d("MyLog", "failed to read file "+filename + ": "+e.getMessage());
        }
        return "";
    }
}
