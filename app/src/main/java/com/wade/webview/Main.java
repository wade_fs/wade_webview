package com.wade.webview;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Main extends AppCompatActivity {
    private final static String JavaBind ="Android";
    private String url = "";

    WebView myBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "請從設定打開儲存權限", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            }
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        myBrowser = (WebView)findViewById(R.id.webview);
        setBrowser();
        Intent rIntent = getIntent();
        Uri uri = rIntent.getData();
        if (uri == null) url = "";
        else {
            url = rIntent.getData().toString();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (url.length() == 0) {
            copyWWW();
            if (checkWWW())
                url = "file://" + Environment.getExternalStorageDirectory() + "/html/index.html";
            else
                url = "https://www.google.com";
        }
        myBrowser.loadUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.view_sourcce:
                Intent intent = new Intent(this, HtmlEditActivity.class);
                intent.putExtra("url", Uri.parse(myBrowser.getUrl()).getPath());
                startActivity(intent);
                return true;
            case R.id.refresh:
                myBrowser.loadUrl(url);
                break;
        }
        return false;
    }

    private int checkAssets() {
        AssetManager am = getResources().getAssets();
        List<String> files = null;
        try {
            files = Arrays.asList(getResources().getAssets().list(""));
            if (files.contains("html.zip")) {
                return 1;
            }
            if (files.contains("index.html")) return 2;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
    private boolean checkWWW() {
        File www = new File(Environment.getExternalStorageDirectory()+"/html/index.html");
        if (www.exists()) return true;
        else return false;
    }
    private void copyWWW() {
        if (!checkWWW()) {
            switch (checkAssets()) {
                case 1:
                    try {
                        InputStream _inputStream = getAssets().open("html.zip");
                        String outpath = Environment.getExternalStorageDirectory().getPath();
                        try {
                            ZipInputStream zin = new ZipInputStream(_inputStream);
                            ZipEntry ze = null;
                            while ((ze = zin.getNextEntry()) != null) {
                                if (ze.isDirectory()) {
                                    _dirChecker(outpath + "/" + ze.getName());
                                } else {
                                    FileOutputStream fout = new FileOutputStream(outpath + "/" + ze.getName());
                                    byte[] buf = new byte[1024];
                                    for (int c = zin.read(buf); c != -1; c = zin.read(buf)) {
                                        fout.write(buf, 0, c);
                                    }

                                    zin.closeEntry();
                                    fout.close();
                                }
                            }
                            zin.close();
                        } catch (Exception e) {
                            warn("複製檔案失敗:" + e.toString(), 0);
                        }
                    } catch (FileNotFoundException e) {
                        warn("html.zip 不存在", 1);
                    } catch (IOException e) {
                        warn("無法找到 html.zip 資源檔: " + e.toString(), 1);
                    }
                    break;
                case 2:
                    final File html = new File(Environment.getExternalStorageDirectory().getPath() + "/html");
                    html.mkdir();
                    InputStream fin;
                    FileOutputStream fout;
                    try {
                        fin = getAssets().open("index.html");
                        String outpath = Environment.getExternalStorageDirectory().getPath();
                        fout = new FileOutputStream(outpath + "/html/index.html");
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = fin.read(buf)) > 0) {
                            fout.write(buf, 0, len);
                        }
                        fout.close();
                        fin.close();
                    } catch (IOException e) {
                        warn("cannot copy file", 1);
                    }
                    break;
                default:
                    warn("Unknown!", 1);
            }
        } else {
            warn(Environment.getExternalStorageDirectory()+"/html/index.html exists", 1);
        }
    }
    private void _dirChecker(String dir) {
        File f = new File(dir);
        f.mkdirs();
    }
    private void setBrowser() {
        myBrowser.addJavascriptInterface(new WebAppInterface(this), JavaBind);
        WebSettings mWebSettings = myBrowser.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setAllowFileAccessFromFileURLs(true);
        mWebSettings.setAllowUniversalAccessFromFileURLs(true);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setDefaultFontSize(14);
        myBrowser.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        myBrowser.setScrollbarFadingEnabled(false);
        myBrowser.setVerticalScrollBarEnabled(true);
    }

    // 0 : both, 1: log.d, 2: Toast
    public void warn(String msg, int mode) {
        if (mode != 2) Log.d("MyLog", msg);
        if (mode != 1) Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
    // 底下是從 javascript 要往 java 呼叫函數時使用，
    // 在 javascript 中大約等於 Android.XXX(), where XXX 定義在底下
    //
    // 如果要從 APK 來呼叫 JavaScript, 則簡單的透過 myBrowser.loadUrl("javascript:YYY(....)");
    // 其中 YYY 定義在 javascript 中
    public class WebAppInterface {
        Context context;
        WebAppInterface(Context c) {
            context = c;
        }

        @JavascriptInterface
        public void showKeyboard() {
            ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        @JavascriptInterface
        public void showToast(String toast) {
            warn(toast, 0);
        }
    }

    private class ArticleWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (myBrowser.canGoBack()) {
                myBrowser.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
